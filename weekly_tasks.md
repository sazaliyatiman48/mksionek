<details>

<summary> <b> Template </b> </summary>
# Date

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] 
- [ ] 
- [ ] 
- [ ] 

</details>
# 2023-02-06

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] compensation conversations
- [ ] planning
- [ ] OKRs
- [ ] handbook changes

<details>

<summary> <b> Every week tasks: </b> </summary>

***Monday:***
- Check error budget
- triaging
- OKRs update
- Async weekly

***Tuesday:***

- Abdul 1:1 prep
- Serena 1:1 prep
- Manoj 1:1 prep
- Peter 1:1 prep
- Hitesh 1:1 prep
- Christina 1:1 prep
- Mansoor 1:1 prep

***Wednesday:***

- Michelle 1:1 prep

***Thursday:***
- Focus work

***Friday:***
- Focus work
- Alex 1:1 prep

</details>


<details>
<summary> <b> Older entries </b> </summary>
# 2023-01-29

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :palm_tree:

Friday - :palm_tree:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] Answer all the pings
- [x] prepare entry for newsletter

# 2023-01-23

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] finish teamops
- [x] feedback sessions
- [x] Finalize OKRs
- [x] break down issues

# 2023-01-16

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] IMOC
- [x] plan feedback sessions
- [x] promo doc
- [x] teamops training

# 2023-01-09

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :sunny:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] compa changes
- [ ] promo doc
- [ ] planning feedback sessions
- [x] error budget

# 2023-01-02

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :ferris_wheel:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] PI review
- [x] review compensation changes
- [x] prepare for delivering performance reviews
- [x] follow up after survey
- [x] teamops training


# 2022-12-26

**My availability:**

Monday - :ferris_wheel:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :sunny:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] team ops training
- [x] survey analysis
- [x] promo doc

# 2022-12-19

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :ferris_wheel:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] promo doc
- [x] instructions for workspace
- [x] handbook fix
- [x] clickhouse issue
- [x] feature flags

# 2022-12-12

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] Add perf reviews to Workday
- [x] edit interviews
- [x] add handbook page
- [x] send survey
- [x] count output for the last 3 milestones

# 2022-12-05

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] Perf reviews
- [x] I am Remarkable workshops
- [x] List of features for our group   
- [x] Planning

# 2022-11-28

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] IMOC shift (Wednesday, Thursday, Friday, Saturday)
- [x] Performance reviews
- [x] Start planning interviews for EM section (invites)
- [x] I am remarkable prep 

# 2022-11-21 - Async week

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :sunny:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] Performance reviews
- [x] count capacity for the next milestone

# 2022-11-14

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] IMOC shift (Friday-Sunday)
- [x] Performance review (MK)
- [x] Performance review (HR)
- [ ] Performance review (AP)

# 2022-11-07

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :ferris_wheel:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] OKRs
- [ ] Planning
- [ ] performance review

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

**If time permits:**
- [ ]


# 2022-10-31

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] catching up after PTO
- [x] Workday assessments
- [x] OKRs

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [ ] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [ ] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [ ] bugs squashing check
- [x] Focus work

**If time permits:**
- [ ]

## 2022-03-10

**My availability:**

Monday - :palm_tree:/:computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :sunny:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] calibrations
- [x] prepare for planning
- [x] IAmRemarkable workshops

**Every week tasks:**

***Tuesday:***

- [x] Async weekly
- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [ ] bugs squashing check
- [ ] Check recruitment
- [ ] Check error budget
- [x] triaging
- [ ] OKRs update
- [ ] prepare myself for career development discussion

**If time permits:**
- [ ]

## 2022-09-26

**My availability:**

Monday - :computer:/:sunny: (half day working)

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :palm_tree:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] more work on performance reviews
- [ ] finish dev on call planning

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [ ] Focus work

***Friday:***
- [ ] Focus work
- [ ] bugs squashing

## 19.09.2022

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] performance reviews
- [x] IMOC SHIFT for 8 days
- [ ] prepare rails girls presentation

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work

**If time permits:**
- [ ]

## 12.09.2022

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] planning
- [x] api discussions
- [x] dev on call
- [x] slowly start with performance reviews

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work


### 05.09.2022

**My availability:**

Monday - :computer:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [ ] backlog
- [x] codeowners
- [x] 360 feedback
- [ ] API problem

**Every week tasks:**

***Monday:***
- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly

***Tuesday:***

- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Christina 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work

### 30.08.2022

**My availability:**

Monday - :sunny:

Tuesday - :computer:

Wednesday - :computer:

Thursday - :computer:

Friday - :computer:


:computer: - working
:ferris_wheel: - public holiday
:palm_tree: - OOO
:sunny: - Family&Friends Day
:thermometer: - Sick day

**My goals for this week:**
- [x] Break down of two issues from Christina
- [x] Promo doc
- [x] backlog
- [x] script for counting closed bugs
- [x] api problem

**Every week tasks:**

***Tuesday:***

- [x] Alex 1:1 prep
- [x] Check recruitment
- [x] Check error budget
- [x] triaging
- [x] OKRs update
- [x] Async weekly
- [x] Engineering allocation meeting prep
- [x] Abdul 1:1 prep
- [x] Serena 1:1 prep
- [x] Manoj 1:1 prep
- [x] Peter 1:1 prep
- [x] Hitesh 1:1 prep
- [x] Mansoor 1:1 prep

***Wednesday:***

- [x] Michelle 1:1 prep

***Thursday:***
- [x] Focus work

***Friday:***
- [x] Focus work

<details>