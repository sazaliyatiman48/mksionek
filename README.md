

## About me



I have worked as a software developer since 2014. I am coding bootcamp alumni; I also have a master's degree in Psychology from the University of Warsaw. Before GitLab, I worked for various start-ups.



Besides work, I like reading, long walks, and enjoy good tv series. I can talk about productivity, mental health and self-care at any time. I wear colourful socks every day.



I live in Warsaw, Poland.



## How to work with me


I work from 10 am to 6 pm local time (CET or CEST in summer, +1 UTC or +2 UTC). I can work later if needed, but not every day. Meetings earlier are hard: I'm not a morning person ;).



I respond on Slack and try to follow up with all my to-dos.



My brain needs time to process information, so I would appreciate it if a meeting had an agenda way before to prepare. This way, I can process the data and bring the most to the meeting. This also means that I often follow up after a meeting to add something.



I believe in clear feedback but deliver with kindness. My role-model Brene Brown says about feedback: clear is kind, unclear is unkind.



I like asking questions and understanding the problem better. I believe we should focus on consistent user experience more - iteration value means we should come back to already developed features often.



I am new to being an Engineering Manager and still learning a lot every day. My favourite part of it is interacting with others more - I missed that when working as an IC.



## Personality tests insights

-   Myers Briggs - Defender (ISFJ-T)

- Strengths Finder ([CliftonStrengths](https://www.gallup.com/cliftonstrengths/en/253676/how-cliftonstrengths-works.aspx))

	1.  Harmony
	2.  Restorative
	3.  Empathy
	4.  Input
	5.  Responsibility
	6.  Developer
	7.  Context
	8.  Individualization
	9.  Learner
	10.  Intellection
