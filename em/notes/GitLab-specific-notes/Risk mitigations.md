understanding employment law of different countries: crucial

https://about.gitlab.com/handbook/legal/employment-law/#basic-employment-laws

looking at the whole arc: hiring, working, anti-harassment, separation, undeperformance, long leave 

do not use protected information (for example: family status) in the interview or hiring decisions. Steer the conversation into the job function requirements


underperformance: 
- blind spots - people not seeing their opportunities
- other team members are suffering - high-performers leave when underperformance is not managed
- skills and behaviours matter! 
- "is there anything that impacting your ability to perform?"
- 1:1 - documenting feedback, coaching for improvement
- sustainable improvement
- if nothing is changing: team member relations


- ask if good time
- do not make assumptions
- how I can support you
- absence management
- https://youtu.be/-mLpytnQtlY

extended leaves: -   Recognize the potential need for a leave, and direct the team member to contact `leaves@gitlab.com` or escalate any concerns to the Absence Management team.

health problems: absence management!! specifically longer than 5 days - in case they need more. parental leave as well. 
https://about.gitlab.com/handbook/paid-time-off/#returning-to-work-after-parental-leave