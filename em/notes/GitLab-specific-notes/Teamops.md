
- improve decision making
- decentralized decision making at a centralized level
- supporting self-leadership
- four guiding principles: 
	- Shared reality
		- speed of knowledge retrieval
		- default to transparency
	- everyone contributes
		- everyone can consume information and contribute
	- Decision velocity
		- this is a key
	- Measurement clarity
		- achieving objectives
		- understanding where you are
		- measuring
		- measure output
		- execution is not a one time event, it establishes new baseline

Leverage teamops, but resist the urge to be perfect. 

IC: better at managing their own time and attention

Prerequisites for TeamOps: 
- communication guideliness
	- robust
	- no unwritten rules
- Shared set of values
	- actionable
	- clearly communicated
- team trust
	- assume positive intent
	- along the way, when it is hard
- Focus on results
	- measuring output
- culture of belonging
	- psychological safety


### Shared reality

teamwork must be based and informed by an objective and shared reality
- information
- objectives
- values
Reality is preserved for continuos and universal access and accountability through documentation: SSOT. 

1) public by default
	- explicitly not public
	- How to create a system that allows everyone to access information? 
	- **management role is to educate team members to use information system and self-serve**
	- ssot: sealing leadership (?)
	- ssot maintained actively by all

#### low context communication
assume person has little knowledge of the topic. Provide all the resources - explicit, direct, simple, comprehensive. 

#### say why, not just what
articulate reasoning behind a decision: prevents speculation and build trust, good for documenting for the future. 

#### situational leadership
assesing the unique situation using different factors

adapt your way

adjusting behaviour from scenario to scenario

#### shared values
must be operationalized 

#### inclusivity
bias for async fosters inclusivity

## Everyone contributes

public by default

right systems

short toes

async workflows

MR to everything

#### DRI - directly responsible individual 
empowered

able to escalate to unblock

collaborate with others

disagree, commit and disagree

#### give agency
manager of one approach 

yet collaborate 

#### key review meetings 
short toes approach 

decisions are two way doors

**informal communication to build trust** 

## Decision velocity 

setting standards on how decisions are made 

internal communications guidelines

push decisions to the lowest possible levels

bias for action

boring solution 

stable counterparts

*collaboration is not consensus*

only healthy constraints

reduce inefficient process

## Measurement clarity 

measuring productivity, value, results

KPI: key performance indicators 

objectives and key results 


KPI and OKRs: symbiotic

OKRs should support KPIs

Measure results, not hours

executing on business results 

Prioritize due dates over scope, cut scope, do not touch dates! 

iteration


# Pilot programme

learners: define, develop, evangelise

shape team-ops

Standarize

growth opportunity 

better team member

Model the behaviour: poposals, facilitate the adoption, external evangelism

how to talk about TeamOps

it's about HOW we are working 

operational model

process: what the process is, predictable results

TeamOps is cyclical

TeamOps is measurable 

Universal information access, knowledge management



