# Management things
#### Grow your teammates
Managers are there to navigate storms by helping the team grow into their roles

1) mentoring: lending advice based on own experience
	1) some advice can land better with one person than another
	2) check in if you're right 
	3) be careful with urg
2) coaching: asking open questions to help reflect and introspect
	1) Reflecting: telling them what you see and hear to tell them reflect
	2) avoid: 
		1) how questions: problem solving mode
		2) why questions: judgy 
		3) ask from the place of genuine curiosity: help people feel, be seen and heard
			1) actual curiosity, not judgy
		4) "What I'm hearing it's... is that right"
		5) holding metaphorical mirror
		6) help your teammates develop their own brain wrinkles :)
3) sponsoring: finding good opportunities to level up and get promoted
	1) adding someones name for opportunities
	2) help get visibility and good assignments
	3) assigning stretch tasks just beyond their skill set
	4) urg members are under-sponsored but overmentored
4) deliver feedback
	1) Regularly delivered
	2) clear picture what is working
	3) specific
	4) actionable
	5) delivered in a way that is possible to absorb
	6) job description as a central point
	7) actionable feedback = observation of behaviour + impact of behaviour + request/question
		1) describe simple facts
		2) sharing the impact beyond surface level feelings, focus on the effect
		3) request - but better a question
		4) good sponsorship opportunities after the feedback
Delivering feedback about the behaviour that was not witnessed directly: harder. Focus on the aspect that you can personally own. 

coach others on delivering feedback! 

1:1 goals: 
- build trust
- gain shared context
- plan out and support career growth
- solve problems
coaching and open questions are better for building trust and helping someone grow

balance those activities

do not pass problematic team mate to another manager!!


Document how team shoudl collaborate and communicate, document team's vision
this would fulfill PREDICTABILITY, BELONGING, EQUITY/FAIRNESS needs

Roles and responsibilities: job description and project specific expectations

Rescponsibility assignment matrix: RACI matrix
- responsible: who do the work
- accountable: who ensures things are done timely and quality is there and communicate to the stakeholders
- consulted
- informed

Responsibility Venn diagram: Who does what and what is shared - helpful when multiple people with different roles work toward common goal

TEAM VISION AND PRIORITIES
- north star
- OKR/KPI

Vision => Mission => Objectives => Strategy
Each one more granular
Strategy is measurable goals

Document team practices 
- meetings (description, time, who should come, goals)
- email group
- slack channels

Empower all to help shape how the team works together
team holding each other accountable
Regularly ask for feedback for the processes and for documentation
Setting expectations will build solid foundations for team's growth


Reference: [[202205291346-resilient-management]]
