# team communication - 2022-06-03
#### Communication
strategic information: communication plan 
- the what
- the why
- who knows
- who will be impacted
- what will be said where and when
- talking points

when communicating - think of which BICEPS needs can be threaten, what questions can be asked, how will you help them get onboard. 

- map big changes back to the things people care about
- choose planned words carefully
- plan out who can be informed when 
- optimize for creating clarity and transparency
- remember that reactions of others can threaten your amygdala - engage your prefrontal cortex

when you disagree and can't commit - be transparent and professional about it 
ask questions, give feedback, share the potential negative things

disagree and commit - put reservations on hold OR leave. 

Meetings are not bad for communicating
recap emails after the meeting
remember about context
Decide which energy this communication requires (color-communication classification)

***listen what people care about and optimize for and craft your message accordingly***

#### Resiliency
change will happen
amygdala hijacked: hard to process new information 

someone going through something: 
	- Describe clearly expectations
	- know your company benefits well
	- lead by example
	- ask for input
	- keep setting expectations - clarity

ASK: if they anticipate a dip in productivity, consider asking them what we can do to meet this goal? 

do not require them to reassure you 
remind people of available resources when something is going on 
be careful about your own energy levels - you can color-code your calendar with the types of things you're doing - and which brains are you using
be aware of what kind of energy certain meetings require

if you delegate hard project to a team - don't make it all easy on them
- tell them how will you support them
- tell them you know it will be hard
- Use a RACI matrix - you are consulted and accountable

Stay in coaching mode to help others grow


Build a support network, focus on people with skills different than yours, 
make it easy for people to say yes, be specific with your ask

Strong connection with your peers is your number one priority



Reference: [[202205291346-resilient-management]]
