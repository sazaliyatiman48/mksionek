## Thanks for feedback
reference: https://www.goodreads.com/book/show/18114120-thanks-for-the-feedback

how to receive feedback
key player is the receiver

the real leverage is creating PULL. 

you are the most important in the learning process

tough feedback is important, yet we feel like we need to run from it

feedback is between two needs: drive to learn and our longing for acceptance

seeking negative feedback is associated with higher performance. It's better to work with people who are open. 

accepting input from spouse: key to the healthy marriage - Gottman

identify your triggers about feedback: 
- truth trigger: when something is not true
- relationship trigger: about relationship with a giver
- identity trigger: feeling threatened, ashamed


### truth trigger
Problem is to understand it well enough to evaluate it

separate: 
- appreciation
- coaching (here is a better way to do it)
- evaluation (here is where you stand)

to understand, ask for clarification

be careful about blind spots! 

disentange what from who

dynamic switchtracking: from what to who

feedback sometimes is created not only in the context of the relationship, but because of it

feedback is about the story in the relationship system

understand the dynamic, keep feedback in the perspective and do not distort feedback. 
Learn how to feel feedback, cultivate the growth identity

Separate 
- appreciation
	- matters
	- being seen matters
	- without it coaching won't get through
	- need to be specific, genuine
		- that receivers values and sees clearly
- coaching
	- help someone learn, grow, change
	- accelerate rating
- evaluation
	- tells where you stand
	- aligns expectations
	- to feel reassured or secure
	- without it we read too much into coaching and appreciation


it's messy if one feedback is evaluated as another, but there is always a bit evaluation in coaching

Discuss the purpose of the feedback specifically: what is the purpose? is it the right purpose from our points of view

to understand, go beyond the labels

where the feedback is coming from and where it is going to

feedback is ??? in observation of a giver and their interpretations

Human minds are organized around stories 

when receiving evaluations: clarify expectations and consequences

have good questions prepared before evaluation conversations (it's hard on the spot)

you can have follow up conversations

shifts from "that's wrong" to "tell me more" => let's figure out why we see things differently

1) different data - matter of access to the data
2) biases driven data collection
	1) seeking data that confirms preexisting view of us
	2) we have biases of our own

Question of interpretation of data: 
- different understanding of how things should be 
- understanding imlicit rules that are in the environment

find out what is right with the feedback, what makes sense

sometimes feedback is just not right - understand why you are rejecting it

you can share your reaction

*dig around the label to understand the message better*

if you don't understand something: slow down, ask, clarify

- understand where feedback is coming from and going to
- understand difference in data and interpretation

gap map: we think => intentions => behaviour => impact => their story about me => feedback

our own expectations and values shape how we see others

misalignements, blind spots: 
- leaky face: we communicate a lot through facial expressions
- leaky tone
- leaky patterns - we sometimes perceive situation differently 

amplifiers: dynamics that amplify the gap between our blind spots and the views of others 

emotional math: sometimes we don't see all aspects of our behaviour 

1) situation vs character: we attribute our actions to situations; of others: to character
2) impact vs intent: we judge ourselves by our intentions, others by the impact

feedback is usually filled with assumed intentions

what helps us to see the blind spots? 
- use your reaction as a blind spot alert
- ask: how do I get in my own way? 
- look for patterns
- get a second opinion (honest vs reassuring, supportive)

focus on change from inside out. Acknowledging the feedback and the behaviour

### relationship triggers

switchtrack conversation: every person is talking about something else

switchtracking defeats feedback

two triggers: what we think about the giver and how we feel treated by them 

how, when, why not working

credibility

trust: their motives are the suspect

how we feel treated by them? 
- appreciation
- autonomy
- acceptance

SPOT TWO TOPICS, signpost those two topics to make it clear

EACH TOPIC GETS THEIR OWN CONVERSATION

sometimes we hide our feelings behind tips and coaching

see relationship system: each of us is the part of the problem and we see only parts of others

three steps back: 
1) you and me intersection
2) role clashes
3) the big picture

- blame absorber: it's all me, cuts feedback short, it lets other off the hook, opportunity to learn lost, building resentment
- blame shifters: it's not me

when we don't understand the system, the fixes will fail - they don't look at the whole system, just one part. 

take responsibility for your part, say what would help you change. 

me + everybody intersection: same feedback from everybody

### identity triggers

feedback threatens our relationship with ourselves

understanding your emotional reactions

1) baseline: personal level of well-being (positive/negative people: different reactions)
2) swing: how high or down you go after the feedback - people are high and low reactive. Bad is stronger than good
3) sustain and recovery: how long the swing last - positive or negative

feelings distorts our stories (exaggerate feedback)

flooding: one information leads to generalization

the forever bias makes the future looks bleak

Dismantle distortions: 
1) know your patterns, prepare, be mindful
2) think through the negative outcome
3) self-observations
4) separate feeling ; story; feedback


Feedback containment chart: 
- what the feedback is about? 
- what the feedback is not about? 

balance chart: right size future consequences, realistic and healthy sense of feedback

- imagine you're an observer
- look back from the future
- use humour

we cannot control how others think of us and they don't think about us that often

others views are input, not imprint

cultivate growth identity: identity is our story about ourselves

give up simple identity labels, embrace complexity

move from fixed mindset to the growth mindset



Learn how wiring and temparament affect your story 

Baseline
- swing (how far you go)
	- bad is stronger than good
- sustain and recovery - how long does the swing last


50-40-10 happiness explanation (wiring, interpretation and reaction, circumstances)

thoughts and feelings - stories 

when you feel lousy about yourself, you see only the evidence of that in your past

flooding: generalising when in a bad mood

future: forever bias

 how to dismantle distortions: 
 1) be prepared, be mindful, know your footprint
 2) inoculate yourself against the worst (and think about the plan b)
 3) notice what's happening with you

Separate feeling/story/feedback

contain the story 

what the feedback is about and what is not about

right-size the future consequences

- imagine you're an observer
- look back from the future

Accept that you can't control how others see you

CULTIVATE GROWTH IDENTITY

identity is our self-story

- give up simple identity labels and cultivate complexity
- move from a fixed mindset to a growth mindset

all-or-nothing: either exagerate feedback, or deny it ☠

identity in black-and-white terms ☠

accept about yourself: 
- you will make mistakes
- you have complex intentions
- you have contributed to the problem 


you can't be perfect and outrun the feedback, you won't escape it. 

accepting imperfection is the only option. 

fixed mindset 👎 growth mindset 👍

part of learning and growing is having a decent handle of your current capabilities - people with growth mindset are better at this

growth mndst folks are better in accepting contradictions

growth mndst: better answer to failure

move toward the growth identity

1) sort feedback toward coaching 
2) hear coaching as coaching 
3) do not oversort toward evaluation: do not create imaginary challenges

evaluation: assessment, consequences, judgement

give yourself a second score: situation itself => how you handle it => second outcome


How good do I have to be? enough is enough, boundaries

1) I may not take your advice
2) I don't want the fedback about this subject, not right now
3) stop, or I will leave the relationship 

Do they attack your character? Is the feedback unrelenting? 

if you change, is there always more demands? 

Does the feedback giver take the relationship hostage? 

warnings or threats? 

is it always you who have to change? 

are your views and feelings a legitimate part of the relationship? 

turning away feedback with grace and honesty: 
- tell them you are rejecting it and what is happening 
- be firm and appreciative
- redirect unhelpful coaching 
	- when you share the complexity or confusion, use "AND statement" - you have listed the input and have decided to go in another direction
	- be specific about the request
	- when settings boundaries, be specific about 3 things: 
		- the request
		- the time frame
		- their assent - make sure they are commited
	- describe consequences
- you have a duty to mitigate the cost to others, if they suffer consequences 
	- acknowledge the impact
	- coach them to deal with unchanged you 
	- problem solve together

### Navigate the conversation

recognize 'keyframes' in the conversation 

phases: 
- open 
- body (listening, asserting (talking), managing the conversation process and problem solving)
- close: clarify commitments, action steps, benchmarks, procedural contracts, follow-ups. 

1) recognize type of feedback, be alert for mixes
2) who decides? 
3) is this final or negotiable

you can influence the agenda: - can we take a step back to talk about the purpose 

Four skills of managing the conversation: 
1) listening - claryfing questions etc
2) asserting - talking
3) process moves
4) problem solving

We need to listen for what is right and be curious about why we see things differently. 

Before feedback: get a talk with your internal voice. 
find your trigger patterns
negotiate with your inner voice

listening rewards the giver's effort, interrupting to clarify is the sign you are listening well. 

Assert to offer your piece of the puzzle. your assertions aren't replacing their truth with your truth, but adding what was left out. 

say you disagree rather that something is "wrong".
you can acknowledge what you are feeling. 
acknowledge your role in the problem even if there are also external things going on. 
give yourself a break if you're overwhelmed. 

supercommunicators are seeing where the conversation is going wrong and are able to correct it. They are seeing the process as well and are able to help the conversation

Process moves: diagnose, describe, propose

listen to the interest behind the feedback, dig to the underlying interest

sources of interest: helping you/helping themselves and the relationship/helping organization 

talk about options, close with commitment (action plans, benchmark and consequences, procedural contracts, new strategies)

ask about one thing you can do or one thing that you do or fail to do that gets in your way
listen for themes
ask what matters to THEM
try small experiments - try the feedback out, especifically when the stakes are low and potential upside is great. Try it on, test it out, it is not all and always. 

increase the positive appeal of change, increase the negative consequences of not changing

J curve: it will get worse before it will get better

request for feedback should be about doing your current role better

the feedback/what is wrong with feedback/what might be right

when you offer something, explain drawbacks.

**coaching is a relationship, not a meeting**

if you want to make learning important, that needs to embedded in the culture
share mistake stories

discuss second scores!! it's more important that the first ones

model learning for others, request coaching
we avoid giving critical feedback, but it hurts others







